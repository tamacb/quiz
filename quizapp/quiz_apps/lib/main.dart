import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'quiz_core.dart';

QuizCore quizCore = new QuizCore();

void main() {
  runApp(Quiz());
}

class Quiz extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: QuizPage(),
          ),
        ),
      ),
      title: 'Quiz App Sample',
//      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Icon> score = [
    Icon(
      Icons.check,
      color: Colors.green,
    ),
  ];

  int nomerPertanyaan = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: new Text(
              quizCore.Banksoal[nomerPertanyaan].isiPertanyaan,
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: 20.0
              ),
            ),
            width: 300.0,
            height: 430.0,
            color: Colors.white70,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: FlatButton(
                textColor: Colors.black,
                color: Colors.green,
                child: new Text(
                  'True',
                  style: TextStyle(
                    fontSize: 17.0,
                  ),
                ),
                onPressed: () {
                  bool cekJawaban = quizCore.Banksoal[nomerPertanyaan].jawaban;
                  if (cekJawaban == true){
                    score.add(Icon(
                      Icons.check,
                      color: Colors.green
                    ));
                    print("jawaban benar");
                  } else {
                    print("jawaban salah");
                  }
                  setState(() {
                    nomerPertanyaan++;
                    print(nomerPertanyaan);
                  });
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: FlatButton(
                textColor: Colors.black,
                color: Colors.red,
                child: new Text(
                  'false',
                  style: TextStyle(
                    fontSize: 17.0,
                  ),
                ),
                onPressed: () {
                  bool cekJawaban = quizCore.Banksoal[nomerPertanyaan].jawaban;
                  if (cekJawaban == false){
                    score.add(Icon(
                        Icons.check,
                      color: Colors.green
                    ));
                    print("jawaban benar");
                  } else {
                    print("jawaban salah");
                  }
                  setState(() {
                    nomerPertanyaan++;
                    print(nomerPertanyaan);
                  });
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: score,
            ),
          )
        ],
      ),
    );
  }
}
